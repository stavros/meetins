from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from djangoql.admin import DjangoQLSearchMixin  # type: ignore

from .models import Item
from .models import Meeting
from .models import User

admin.site.register(User, UserAdmin)


class ItemInline(admin.StackedInline):
    model = Item


@admin.register(Meeting)
class MeetingAdmin(DjangoQLSearchMixin, admin.ModelAdmin):
    list_display = ["id", "organizer", "created", "title"]
    list_filter = ("created",)
    search_fields = ("title",)
    inlines = [ItemInline]


@admin.register(Item)
class ItemAdmin(DjangoQLSearchMixin, admin.ModelAdmin):
    list_display = ["id", "meeting", "planned_duration"]
    search_fields = ("description", "notes")
