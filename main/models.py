from django.contrib.auth.models import AbstractUser
from django.db import models
from django.urls import reverse
from shortuuid.django_fields import ShortUUIDField  # type: ignore


class User(AbstractUser):
    pass


class Meeting(models.Model):
    id = ShortUUIDField(
        length=8,
        max_length=200,
        primary_key=True,
    )
    created = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=2000)
    # Meetings with no organizer set are stored in the user's session, and can only be
    # accessed from it.
    organizer = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self) -> str:
        return self.title

    def get_absolute_url(self):
        return reverse("main:meeting", args=[self.id])


class Item(models.Model):
    id = ShortUUIDField(
        length=8,
        max_length=200,
        primary_key=True,
    )
    meeting = models.ForeignKey(Meeting, on_delete=models.CASCADE, related_name="items")
    description = models.TextField(help_text="The description of this item.")
    notes = models.TextField(
        help_text="Notes or decision taken on this agenda item.", blank=True
    )
    introducer = models.CharField(max_length=2000, blank=True)
    planned_duration = models.IntegerField(
        null=True,
        blank=True,
        help_text="The time this item should roughly take (in seconds).",
    )
    actual_duration = models.IntegerField(
        null=True,
        blank=True,
        help_text="The time this item actually took (in seconds).",
    )

    def __str__(self) -> str:
        return self.id
