from django.http import Http404
from django.http import HttpRequest
from django.http import HttpResponse
from django.shortcuts import render

from main.models import Meeting


def index(request):
    return render(request, "landing.html")


def meeting(request: HttpRequest, meeting_id: str) -> HttpResponse:
    meeting = Meeting.objects.filter(pk=meeting_id).first()
    if not meeting:
        raise Http404

    return render(request, "meeting.html", {"meeting": meeting})
