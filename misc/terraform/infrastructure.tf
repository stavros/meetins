terraform {
  backend "http" {
  }
}

terraform {
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 2.0"
    }
  }
}

provider "cloudflare" {
  api_token = var.cloudflare_token
}


variable "cloudflare_token" {}
variable "zone_id" {
  default = "55dd43c1c4a2af51f6f69903d1bbb278"
}

variable "ipv6_ip" { default = "2a01:4f8:1c0c:6109::1" }
variable "ipv4_ip" { default = "195.201.40.251" }
variable "domain" { default = "meetins.co" }

resource "cloudflare_record" "v6_root" {
  zone_id = var.zone_id
  type    = "AAAA"
  name    = "@"
  proxied = "true"
  value   = var.ipv6_ip
}

resource "cloudflare_record" "root" {
  name    = "@"
  zone_id = var.zone_id
  type    = "A"
  proxied = "true"
  value   = var.ipv4_ip
}

resource "cloudflare_record" "v6_www" {
  zone_id = var.zone_id
  type    = "AAAA"
  name    = "www"
  proxied = "true"
  value   = var.ipv6_ip
}

resource "cloudflare_record" "www" {
  name    = "www"
  zone_id = var.zone_id
  type    = "A"
  proxied = "true"
  value   = var.ipv4_ip
}


resource "cloudflare_page_rule" "terraform_managed_resource_3555b20be4daa39bc430feac9dee8bed" {
  zone_id  = var.zone_id
  priority = 1
  status   = "active"
  target   = "meetins.co/*"
  actions {
    forwarding_url {
      status_code = 301
      url         = "https://www.meetins.co/$1"
    }
  }
}
